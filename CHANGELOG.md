# [2.1.0](https://gitlab.com/halimarm/semantic-release/compare/v2.0.0...v2.1.0) (2021-02-21)


### Features

* add feature 4 ([9461a58](https://gitlab.com/halimarm/semantic-release/commit/9461a58ea4b04ffa913b484c31e361e1f3882d37))

# [2.0.0](https://gitlab.com/halimarm/semantic-release/compare/v1.2.3...v2.0.0) (2021-02-20)


* BREAKING CHANGE: upgrade feature v1 to v2 ([cae68dd](https://gitlab.com/halimarm/semantic-release/commit/cae68dd29c3a9f710495b1d979cbc4fea9598bb2))


### Features

* add README.md ([7c0f36b](https://gitlab.com/halimarm/semantic-release/commit/7c0f36b090de8b4250b18066ea02a4dbf03085d8))


### BREAKING CHANGES

* upgrade feature v1 to v2
- Feature 1  - v2
- Feature 2 - v2
- Feature 3 - v2

# [1.3.0](https://gitlab.com/halimarm/semantic-release/compare/v1.2.3...v1.3.0) (2021-02-20)


### Features

* add README.md ([7c0f36b](https://gitlab.com/halimarm/semantic-release/commit/7c0f36b090de8b4250b18066ea02a4dbf03085d8))

# [1.3.0](https://gitlab.com/halimarm/semantic-release/compare/v1.2.3...v1.3.0) (2021-02-20)


### Features

* add README.md ([7c0f36b](https://gitlab.com/halimarm/semantic-release/commit/7c0f36b090de8b4250b18066ea02a4dbf03085d8))

## [1.2.3](https://gitlab.com/halimarm/semantic-release/compare/v1.2.2...v1.2.3) (2021-02-19)


### Bug Fixes

* gitlab-ci ([dc286b8](https://gitlab.com/halimarm/semantic-release/commit/dc286b8f4d42bf4ee407c30085007e0814ec2640))
